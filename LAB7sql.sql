CREATE DATABASE Lab7;
USE Lab7;
CREATE TABLE peopleData
(
	LastName varchar(40),
    FirstName varchar(40),
    PayRate decimal(10),
	HoursWorked decimal(10)
);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Smith', 'Seth', 55.5, 12.50);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Jones', 'Adam', 40.5, 5.50);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Davis', 'Chris', 50.0, 10.00);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Machado', 'Manny', 45.5, 5.50);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Hardy', 'J.J.', 40.0, 17.25);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Schoop', 'Jon', 45.0, 23.80);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Rickard', 'Joey', 40.0, 35.00);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Walker', 'Chris', 25.5, 13.00);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Trumbo', 'Mark', 30.0, 11.00);
INSERT INTO peopledata(LastName, FirstName, PayRate, HoursWorked) Values('Joseph', 'Caleb', 20.5, 10.00);
